import React, { Component } from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.authors=[];
    this.quotes=[];
    this.languages=[];
    this.state.authors = [];
    this.state.languages = [];
    this.state.quotes = [];
  }

  addAuthor = () => {
    let author = {
      name: this.state.name,
      country: this.state.country,
      birth: this.state.birth
    }
    this.authors.push(author);
  }

  handleChangeAuthorName = (event) => {
    this.setState({
      name: event.target.value
    })
  }


  handleChangeAuthorCountry = (event) => {
    this.setState({
      country: event.target.value
    })
  }

  handleChangeAuthorBirth = (event) => {
    this.setState({
      birth: event.target.value
    })
  }

  addLanguage = () => {
    let language = {
      languageName: this.state.languageName,
      isDefault: this.state.isDefault
    }
    this.languages.push(language);
  }

  handleChangeLanguageName = (event) => {
    this.setState({
      languageName: event.target.value
    })
  }


  handleChangeDefault = (event) => {
    this.setState({
      isDefault: event.target.type === 'checkbox' ? event.target.checked : event.target.value
    })
  }

  addQuote = () => {
    let quote = {
      authorId: this.state.authorId,
      languageId: this.state.languageId,
      description: this.state.description,
      year: this.state.year
    }
    this.quotes.push(quote);
  }
  
    handleChangeAuthorId = (event) => {
    this.setState({
      authorId: event.target.value
    })
  }
  
    handleChangeLanguageId= (event) => {
    this.setState({
      description: event.target.value
    })
  }
  
    handleChangeDescription = (event) => {
    this.setState({
      languageName: event.target.value
    })
  }
  
    handleChangeYear = (event) => {
    this.setState({
      year: event.target.value
    })
  }
  

  render() {
      return (
        <div className="row">
       <div className="col-sm-6">
        <div className="addAuthor-form">
        <div className="main-div">
        <h2>Add author</h2>
        <div id="addAuthor">
          <div className="form-group">
              <input type="text" className="form-control" id="authorName" placeholder="Author name" onChange={this.handleChangeAuthorName}/>
          </div>
          <div className="form-group">
              <input type="text" class="form-control" id="countryAuthor" placeholder="Country" onChange={this.handleChangeAuthorCountry}/>
          </div>
          <div className="form-group">
              <input type="number" className="form-control" id="birth" placeholder="Birth" onChange={this.handleChangeAuthorBirth}/>
          </div>
          <button className="btn btn-primary" onClick={this.addAuthor} >Add author</button>
       </div>
       </div>
       </div>
      </div>
      <div className="col-sm-6">
        <div className="addAuthor-form">
        <div className="main-div">
        <h2>Add language</h2>
        <div id="addAuthor">
          <div className="form-group">
              <input type="text" className="form-control" id="languageName" placeholder="Language name" onChange={this.handleChangeLanguageName}/>
          </div>
          <div className="form-group">
              <input type="checkbox" class="form-control" id="default" placeholder="Default" onChange={this.handleChangeDefault}/>
          </div>
          <button className="btn btn-primary" onClick={this.addLanguage} >Add language</button>
       </div>
       </div>
       </div>
      </div>
      <div className="col-sm-6">
        <div className="addAuthor-form">
        <div className="main-div">
        <h2>Add quote</h2>
        <div id="addAuthor">
          <div className="form-group">
              <input type="text" className="form-control" id="description" placeholder="Quote description" onChange={this.handleChangeDescription}/>
          </div>
          <div className="form-group">
              <input type="number" class="form-control" id="year" placeholder="Year" onChange={this.handleChangeYear}/>
          </div>
          <button className="btn btn-primary" onClick={this.addQuote} >Add quote</button>
       </div>
       </div>
       </div>
      </div>
      <div className="col-sm-6">
        <div className="addAuthor-form">
        <div className="main-div">
        <div id="viewItems">
          <div className="form-group">
            <div> Authors: 
              {this.authors.map(function(d, idx){
                 return (<li key={idx}>{d.name + " - " +d.country}</li>)
               })}
           </div>
          </div>
       </div>
       </div>
       </div>
      </div>
      <div className="col-sm-6">
        <div className="addAuthor-form">
        <div className="main-div">
        <div id="viewItems">
          <div className="form-group">
            <div> Languages: 
              {this.languages.map(function(d, idx){
                 return (<li key={idx}>{d.languageName + " - " +d.isDefault}</li>)
               })}
           </div>
          </div>
       </div>
       </div>
       </div>
      </div>
       <div className="col-sm-6">
        <div className="addAuthor-form">
        <div className="main-div">
        <div id="viewItems">
          <div className="form-group">
            <div> Quotes: 
              {this.quotes.map(function(d, idx){
                 return (<li key={idx}>{d.description}</li>)
               })}
           </div>
          </div>
       </div>
       </div>
       </div>
      </div>
      </div>
      );
    }
}


  export default App;
